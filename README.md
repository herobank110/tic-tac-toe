# Tic Tac Toe

A Tic Tac Toe game made in Unreal Engine 4.


## Game Style

The game will have 2 person local multiplayer. For the prototype input will come
from a single mouse, and will later support multiple controllers. A later
version will also support online multiplayer through online API (Steam).

The game will have low quality 3d graphics with a few simple menus. The main
interactions will come from starting a match and gameplay input. This consists
of clicking on the board tile where the player wants to land.


## Development Notice

Now that the project is kinda big, remember to restart Unreal Editor
before play testing after compiling code. Sometimes the hot reload does not
work, even for small changes. Otherwise consider building and debugging the
project directly from the IDE (using UBT).