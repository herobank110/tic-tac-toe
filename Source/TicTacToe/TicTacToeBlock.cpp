// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TicTacToeBlock.h"
#include "TicTacToeBlockGrid.h"
#include "TicTacToeGameState.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"

ATicTacToeBlock::ATicTacToeBlock()
{
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> GreenMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, BaseMaterial(TEXT("/Game/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, BlueMaterial(TEXT("/Game/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			, OrangeMaterial(TEXT("/Game/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
			, GreenMaterial(TEXT("/Game/Puzzle/Meshes/GreenMaterial.GreenMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Create static mesh component
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(1.f, 1.f, 0.25f));
	BlockMesh->SetRelativeLocation(FVector(0.f, 0.f, 25.f));
	BlockMesh->SetMaterial(0, ConstructorStatics.BlueMaterial.Get());
	BlockMesh->SetupAttachment(DummyRoot);
	//BlockMesh->OnClicked.AddDynamic(this, &ATicTacToeBlock::BlockClicked);
	//BlockMesh->OnInputTouchBegin.AddDynamic(this, &ATicTacToeBlock::OnFingerPressedBlock);

	// Create text component
	OwnerText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("OwnerText0"));
	OwnerText->SetRelativeLocation(FVector(0.f, 0.f, 60.f));
	OwnerText->SetRelativeRotation(FRotator(90.f, 180.f, 0.f));
	OwnerText->SetXScale(4.f);
	OwnerText->SetYScale(4.f);
	OwnerText->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);
	OwnerText->SetVerticalAlignment(EVerticalTextAligment::EVRTA_TextCenter);
	OwnerText->SetText(FText::GetEmpty());
	OwnerText->SetupAttachment(DummyRoot);

	// Save a pointer to the orange material
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();
	GreenMaterial = ConstructorStatics.GreenMaterial.Get();
}

void ATicTacToeBlock::BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	HandleClicked();
}


void ATicTacToeBlock::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
	HandleClicked();
}

void ATicTacToeBlock::HandleClicked()
{
	// Check we are not already active
	if (!bIsActive)
	{
		// Tell the game state and check if we should be active now.
		auto const GameState = GetWorld()->GetGameState<ATicTacToeGameState>();
		if (GameState && GameState->LandTile(this))
		{
			bIsActive = true;

			// Change material
			FText NewText;
			switch (GameState->GetCurrentPlayerIndex())
			{
			default:
				NewText = FText::GetEmpty();
				break;
			case 0:  // X for player 1.
				NewText = FText::FromString(TEXT("X"));
				break;
			case 1:  // O for player 2.
				NewText = FText::FromString(TEXT("O"));
				break;
			}
			OwnerText->SetText(NewText);

			// Revert to default unhovered material.
			BlockMesh->SetMaterial(0, BlueMaterial);

			// Tell the Grid
			if (OwningGrid != nullptr)
			{
				OwningGrid->AddScore();
			}
		}
	}
}

void ATicTacToeBlock::Highlight(bool bOn)
{
	// Do not highlight if the block has already been activated.
	if (bIsActive)
	{
		return;
	}

	if (bOn)
	{
		BlockMesh->SetMaterial(0, BaseMaterial);
	}
	else
	{
		BlockMesh->SetMaterial(0, BlueMaterial);
	}
}
