// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TicTacToePlayerController.h"
#include "TicTacToePlayerState.h"
#include "TicTacToePawn.h"
#include "TicTacToe.h"
#include "Engine/World.h"

ATicTacToePlayerController::ATicTacToePlayerController()
{
	// Input setup
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	// Turn based gameplay
	bCanAcceptTurnInput = false;
}

void ATicTacToePlayerController::AcceptInputForTurn()
{
	bCanAcceptTurnInput = true;
}

void ATicTacToePlayerController::StopAcceptingInputForTurn()
{
	bCanAcceptTurnInput = false;
}
