//     Tic Tac Toe    Copyright (C) 2019  David

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TicTacToePlayerState.generated.h"


/**
 * Represent a single player in the game.
 */
UCLASS()
class TICTACTOE_API ATicTacToePlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	/* Set default values */
	ATicTacToePlayerState();

	/**************************
	* Turn Based 			  *
	***************************/

	/* Tell the gamestate to end this player's turn if possible.*/
	void EndMyTurn();

	/*
	* Called when the player turn changes.
	*
	* @param bIsMyTurn Whether it is now this player's turn.
	*/
	void OnTurnChanged(const bool bNewIsMyTurn);

private:
	/* Whether it is currently this player's turn. */
	bool bIsMyTurn;

public:
	/* Returns whether it is my turn. */
	FORCEINLINE bool GetIsMyTurn() const { return bIsMyTurn; };

	
};
