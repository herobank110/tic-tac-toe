// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TicTacToeGameState.h"
#include "TicTacToeGameMode.generated.h"

/** 
* GameMode class to specify gameplay classes and manage
* game configuration options
*/
UCLASS(minimalapi)
class ATicTacToeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATicTacToeGameMode();

	/** Number of local players to create at game start */
	UPROPERTY(EditAnywhere, Category=Game, meta = (ClampMin = 0))
	int32 NumLocalPlayers;

	/**
	* Use the player start with corresponding PlayerStartTag
	* to the player index (0, 1).
	*
	* Ensure PlayerStartTags are set as numbers in order of
	* which players to spawn, otherwise random order will be used.
	*/
	virtual FString InitNewPlayer(APlayerController* NewPlayerController, 
		const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal) override;

	/** Set the player name using current player name index */
	virtual void GenericPlayerInitialization(AController* NewPlayer) override;

public:

	/**************************
	* Turn Based 			  *
	***************************/

	/** Return whether the match can begin */
	UFUNCTION(BlueprintNativeEvent, Category=TurnBased)
	bool CanStartMatch();

	/** Return whether the match can end. Should also be able to return results if successful
	from GetMatchResults. */
	UFUNCTION(BlueprintNativeEvent, Category=TurnBased)
	bool CanEndMatch();

protected:
	/* Turn based config variables */

	/* Delay for match to start after it is confirmed, in seconds */
	UPROPERTY(EditAnywhere, Category=TurnBased, meta = (ClampMin = 0.f))
	float ConfirmedMatchStartDelay;

	/** Saved results from last CanEndMatch call. */
	FTicTacToeMatchResults LastMatchResults;

protected:
	// Begin AActor interface
	virtual void BeginPlay() override;
	// End AActor interface

private:
	/** Player start tag of next player start to use */
	int32 NextPlayerStartIndex;

public:
	/* Returns ConfirmedMatchStartDelay */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category=TurnBased)
	FORCEINLINE float GetConfirmedMatchStartDelay() const { return ConfirmedMatchStartDelay; };

	/** Return the results of the last match. Only valid right after calling CanEndMatch
	and it returned true. */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = TurnBased)
	FORCEINLINE FTicTacToeMatchResults GetMatchResults() const { return LastMatchResults; };

};
