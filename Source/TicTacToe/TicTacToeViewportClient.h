//     Tic Tac Toe    Copyright (C) 2019  David

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameViewportClient.h"
#include "TicTacToeViewportClient.generated.h"

/**
 * Handle input for the possibility of local multiplayers with
 * the same input method for multiple players.
 */
UCLASS()
class TICTACTOE_API UTicTacToeViewportClient : public UGameViewportClient
{
	GENERATED_BODY()

public:
	
	// Set default values.
	UTicTacToeViewportClient();

	/*Let all players receive input from the first player controller. */
	virtual bool InputKey(const FInputKeyEventArgs& EventArgs) override;

	/* Name of the action mapping to send to all players. */
	FName ActionToSendToAllLocals;

private:

	bool IsKeyInActionMapping(const FKey& Key, const FName& MappingName) const;

	// This is the old version and will not compile!
	//virtual bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key,
	//	EInputEvent EventType, float AmountDepressed = 1.f, bool bGamepad = false) override;
};
