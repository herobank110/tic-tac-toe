//     Tic Tac Toe    Copyright (C) 2019  David

#include "TicTacToePlayerState.h"
#include "TicTacToeGameState.h"
#include "Engine/World.h"
#include "TurnBasedController.h"
#include "TicTacToe.h"

ATicTacToePlayerState::ATicTacToePlayerState()
	: bIsMyTurn(false)
	{}

void ATicTacToePlayerState::EndMyTurn()
{
	if (bIsMyTurn)
	{
		if (auto GameState = GetWorld()->GetGameState<ATicTacToeGameState>())
		{
			GameState->EndTurn();
		}

		// Tell the controller to stop accepting input (this function may get
		// called many times, but it ensures we don't get accidental input.)
		auto Owner = GetOwner();
		auto OwnerController = Owner ? Cast<ITurnBasedController>(Owner) : NULL;
		if (OwnerController)
		{
			OwnerController->StopAcceptingInputForTurn();
		}
	}
}

void ATicTacToePlayerState::OnTurnChanged(const bool bNewIsMyTurn)
{
	// For networking this should be the server telling the specific client player controller
	// to take input, or running the Ai controller from the server straight away.
	// The owner of a player state is his controller: only valid on server, and 1 client for PlayerControllers
	auto Owner = GetOwner(); 
	auto OwnerController = Owner ? Cast<ITurnBasedController>(Owner): NULL;
	if (!OwnerController)
	{
		return;
	}

	// Set bIsMyTurn variable to incoming value.
	this->bIsMyTurn = bNewIsMyTurn;

	if (bIsMyTurn)
	{
		PIE_INFOF(TEXT("Turn %s's started"), *GetPlayerName());

		// Ask the controller using this player state to take input.
		OwnerController->AcceptInputForTurn();

		// End turn immediately ONLY FOR DEBUGGING!
		//EndMyTurn();
	}
	else
	{
		OwnerController->StopAcceptingInputForTurn();
	}
}
