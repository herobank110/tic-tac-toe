// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TurnBasedController.h"
#include "TicTacToePlayerController.generated.h"

/** PlayerController class used to enable cursor */
UCLASS()
class ATicTacToePlayerController : public APlayerController, public ITurnBasedController
{
	GENERATED_BODY()

public:
	ATicTacToePlayerController();


	// Start of ITurnBasedController interface
	
	/* Enable player input for clicking a tile. */
	virtual void AcceptInputForTurn() override;

	/* Disable player input for clicking a tile. */
	virtual void StopAcceptingInputForTurn() override;

	// End of ITurnBasedController interface

private:
	/* Whether this player is allowed to perform gameplay input. */
	bool bCanAcceptTurnInput;

public:
	/* Returns bCanAcceptTurnInput */
	FORCEINLINE bool GetCanAcceptTurnInput() const { return bCanAcceptTurnInput; };

};


