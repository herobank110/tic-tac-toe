// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TicTacToeBlockGrid.h"
#include "TicTacToeBlock.h"
#include "Components/TextRenderComponent.h"
#include "Engine/World.h"

#define LOCTEXT_NAMESPACE "PuzzleBlockGrid"

ATicTacToeBlockGrid::ATicTacToeBlockGrid()
{
	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Create static mesh component
	ScoreText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("ScoreText0"));
	ScoreText->SetRelativeLocation(FVector(200.f,0.f,0.f));
	ScoreText->SetRelativeRotation(FRotator(90.f,0.f,0.f));
	ScoreText->SetText(FText::Format(LOCTEXT("ScoreFmt", "Score: {0}"), FText::AsNumber(0)));
	ScoreText->SetupAttachment(DummyRoot);

	// Set defaults
	Size = 3;
	BlockSpacing = 300.f;
	BlockClass = ATicTacToeBlock::StaticClass();
}


void ATicTacToeBlockGrid::BeginPlay()
{
	Super::BeginPlay();

	// Number of blocks
	const int32 NumBlocks = Size * Size;

	// Loop to spawn each block
	for(int32 BlockIndex=0; BlockIndex<NumBlocks; BlockIndex++)
	{
		// Calculate 2d grid coordinates of the block.
		const int32 XOffset = (BlockIndex/Size); // Divide by dimension
		const int32 YOffset = (BlockIndex%Size); // Modulo gives remainder

		// Make position vector, offset from Grid location
		const FVector BlockLocation = FVector(XOffset * BlockSpacing, YOffset * BlockSpacing, 0.f) + GetActorLocation();

		// Spawn a block
		auto NewBlock = GetWorld()->SpawnActor<ATicTacToeBlock>(*BlockClass, BlockLocation, FRotator(0,0,0));
		

		if (NewBlock != nullptr)
		{
			// Add the block to the grid.
			const auto NewGridPos = FIntPoint(YOffset, XOffset);
			if (!BlocksMap.IsValidIndex(YOffset)) BlocksMap.AddDefaulted();
			BlocksMap[YOffset].Add(NewBlock);

			// Tell the block about its owner
			NewBlock->OwningGrid = this;
			NewBlock->GridPosition = NewGridPos;
		}
	}
}


void ATicTacToeBlockGrid::AddScore()
{
	// Increment score
	Score++;

	// Update text
	ScoreText->SetText(FText::Format(LOCTEXT("ScoreFmt", "Score: {0}"), FText::AsNumber(Score)));
}

#undef LOCTEXT_NAMESPACE
