//     Tic Tac Toe    Copyright (C) 2019  David

#include "TicTacToeGameState.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "TurnBasedController.h"
#include "TicTacToeGameMode.h"
#include "TicTacToePlayerState.h"
#include "TicTacToeBlock.h"
#include "TicTacToe.h"

ATicTacToeGameState::ATicTacToeGameState()
	: CurrentTurnCount(-1)
	, EndingTurnCount(-1)
	, ActivePlayers()
	, bHasMatchStarted(false)
	, bHasMatchFinished(false)
	{}

bool ATicTacToeGameState::StartMatch()
{
	if (bHasMatchStarted)
	{
		// Only allow one match at a time. Show a warning message if
		// this is attempted.
		PIE_WARNING(TEXT("Attempted to start match while an ongoing match exists"));
		return false;
	}
	else if (MatchStartTimer.IsValid())
	{
		// Only allow one confirmed match at a time.
		PIE_WARNING(TEXT("Attempted to start match while a confirmed match exists"));
		// The match will still start after the confirmation delay.
		// It is counter intuitive to return false here.
		return true;
	}

	auto World = GetWorld();
	auto GM = World ? World->GetAuthGameMode<ATicTacToeGameMode>() : NULL;
	if (GM && GM->CanStartMatch())
	{
		// This will only be valid on the server (if we use networked multiplayer)
		// because GM will be null on clients.

		// Reset track matching variables from last match.
		// Ensure we don't start another match while this is ongoing.
		ReinitializeTurnBased();

		float DelayTime = GM->GetConfirmedMatchStartDelay();
		PIE_INFOF(TEXT("Match confirmed, starting in %.3f seconds"), DelayTime);
		World->GetTimerManager().SetTimer(MatchStartTimer, this, &ATicTacToeGameState::StartConfirmedMatch, DelayTime, false);
		return true;
	}
	else
	{
		PIE_ERROR(TEXT("Match could not be started"));
		return false;
	}
}

void ATicTacToeGameState::CancelConfirmedMatch()
{
	if (!bHasMatchStarted && MatchStartTimer.IsValid())
	{
		// We are dealing with a confirmed match that hasn't started yet.
		PIE_INFO(TEXT("Match cancelled"));
	}
}

void ATicTacToeGameState::EndTurn()
{
	auto GameMode = GetWorld()->GetAuthGameMode<ATicTacToeGameMode>();

	++CurrentTurnCount;
	// Check if this caused the player index to loop back to 0.
	bool const bHasSwitchedRound = GetCurrentPlayerIndex() == 0;

	// Attempt to finish the match.
	if (GameMode && GameMode->CanEndMatch())
	{
		// Get the match results right after calling CanEndMatch so they are valid
		//FTicTacToeMatchResults MatchResults = GameMode->GetMatchResults();		
		auto MatchResults = FTicTacToeMatchResults();
		FinalizeEndMatch(MatchResults);
		PIE_INFO(TEXT("Match ended"));
		return OnMatchEnd.Broadcast();
	}

	if (bHasSwitchedRound)
	{
		// Call delegates bound for when new round has started
		OnRoundEnd.Broadcast();
		PIE_INFOF(TEXT("Round %d started"), GetCurrentRoundIndex() + 1);
		OnRoundStart.Broadcast();
	}

	// Tell players the turn has changed
	int32 Index = 0;
	while (ActivePlayers.IsValidIndex(Index))
	{
		ActivePlayers[Index]->OnTurnChanged(Index == GetCurrentPlayerIndex());
		++Index;
	}
}

bool ATicTacToeGameState::LandTile(ATicTacToeBlock* TileToLand)
{
	if (!HasMatchStarted()) return false;

	if (TileToLand && !TileToLand->OwningPlayer)
	{
		// The tile has no owner. Assign the current player.
		TileToLand->OwningPlayer = GetCurrentPlayer();

		PIE_INFOF(TEXT("Assigned tile (%d %d) to %s"), TileToLand->GridPosition.X,
			TileToLand->GridPosition.Y, *GetCurrentPlayer()->GetPlayerName());

		// Landing a tile will end the player's turn.
		FTimerHandle Timer;
		GetWorld()->GetTimerManager().SetTimer(Timer, this, &ATicTacToeGameState::EndTurn, .01f, false);

		return true;
	}
	return false;
}

void ATicTacToeGameState::ReinitializeTurnBased()
{
	CurrentTurnCount = -1;
	EndingTurnCount = -1;
	LastResults = FTicTacToeMatchResults();
	SetActivePlayers();
	bHasMatchStarted = false;
	bHasMatchFinished = false;
}

void ATicTacToeGameState::SetActivePlayers()
{
	// Clear the players that were there before.
	ActivePlayers.Empty(2);

	// Check the player array for all active player states in the world
	for (auto Player : PlayerArray)
	{

		if (Player->bOnlySpectator || Player->GetPlayerName().IsEmpty())
		{
			// The player state with an empty name is not a valid player.
			continue;
		}

		int32 CurrentIndex = 0;
		auto TicTacToePlayer = Cast<ATicTacToePlayerState>(Player);
		if (TicTacToePlayer)
		{
			ActivePlayers.Add(TicTacToePlayer);
			++CurrentIndex;

			if (CurrentIndex >= 2)
			{
				// Only add up to 2 players
				return;
			}
		}
	}
}

void ATicTacToeGameState::StartConfirmedMatch()
{
	// Clear the timer handle as we are done with it.
	MatchStartTimer.Invalidate();

	bHasMatchStarted = true;
	PIE_INFO(TEXT("Match started"));

	OnMatchStart.Broadcast();
	return EndTurn(); // Starts the first player's turn.
}

void ATicTacToeGameState::FinalizeEndMatch(const FTicTacToeMatchResults& MatchResults)
{
	EndingTurnCount = CurrentTurnCount;
	LastResults = MatchResults;
	bHasMatchStarted = false;
	bHasMatchFinished = true;

	// Inform players that "turn has changed" with no one having their turn.
	for (auto Player : ActivePlayers)
	{
		// It's not their turn (for all players).
		Player->OnTurnChanged(false);
	}
}
