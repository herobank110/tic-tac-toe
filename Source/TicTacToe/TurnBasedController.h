//     Tic Tac Toe    Copyright (C) 2019  David

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TurnBasedController.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTurnBasedController : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface for player and Ai controller who will be used to provide player input
 * to a player in the turn based game. There are two main input modes, where the 
 * turn belongs to the player, and where the turn belongs elsewhere.
 */
class TICTACTOE_API ITurnBasedController
{
	GENERATED_BODY()

public:
	/* Method to enable (and prompt) the controller to provide gameplay input while it is their turn. */
	virtual void AcceptInputForTurn() = 0;

	/* Method to disable the controller to provide gameplay input until requested to do so again. */
	virtual void StopAcceptingInputForTurn() = 0;
};
