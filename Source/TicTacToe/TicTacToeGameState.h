//     Tic Tac Toe    Copyright (C) 2019  David

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TicTacToeGameState.generated.h"

UENUM(BlueprintType)
/* Contains information about how a player won the match. */
enum class ETicTacToeWinMethod : uint8
{	
	None,
	/* An edge-edge horizontal row was made.*/
	Row,
	/* An edge-edge vertical column was made*/
	Column,
	/* A corner-corner diagonal was made.*/
	Diagonal,
};

/* Contains all the information out the outcome of a match after it is finished. */
USTRUCT(BlueprintType)
struct FTicTacToeMatchResults
{
	GENERATED_BODY()

	/* Whether the board was filled and both players drew. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bWasDraw;

	/* The reason a player won (if any). */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ETicTacToeWinMethod WinMethod;

	/* Index of winning row/column/diagonal. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 WinningIndex;

	/* Initialise with no win information (the game was a draw) */
	FTicTacToeMatchResults()
	{
		bWasDraw = true;
		WinMethod = ETicTacToeWinMethod::None;
		WinningIndex = -1;
	}

	/* Initialise with win information (the game was not a draw) */
	FTicTacToeMatchResults(ETicTacToeWinMethod WinMethod, int32 WinningIndex)
	{
		bWasDraw = false;
		this->WinMethod = WinMethod;
		this->WinningIndex = WinningIndex;
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMatchConfirm);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMatchCancel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMatchStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMatchEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRoundStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRoundEnd);

/**
* Determine available game actions based on the current
* state of the game, including game ending conditions.
*/
UCLASS()
class TICTACTOE_API ATicTacToeGameState : public AGameStateBase
{
	GENERATED_BODY()

public:

	/* Set default values */
	ATicTacToeGameState();

	/**************************
	* Turn Based 			  *
	***************************/

	// TODO: set replication for networked multiplayer (server only)
	/* Attempt to start a match with the current settings.
	*
	* @return Whether the match is confirmed and is going to start imminently
	*/
	UFUNCTION(BlueprintCallable, Category=TurnBased)
	bool StartMatch();

	// TODO: set replication for networked multiplayer (server only)
	/* Cancel a confirmed match that has not yet officially started. */
	UFUNCTION(BlueprintCallable, Category = TurnBased)
	void CancelConfirmedMatch();

	// TODO: set replication for networked multiplayer (server only)
	/* End the turn of the current player and progress the turn based game. */
	void EndTurn();

	/* Assign the given tile to the active player and end his turn.
	*
	* @return Whether tile was landed to the active player.
	*/
	bool LandTile(class ATicTacToeBlock* TileToLand);

protected:
	/* Called before match starts to reset turn based tracking from a previous match. */
	void ReinitializeTurnBased();

	/* Set the players who will participate in the upcoming match. */
	void SetActivePlayers();

	/* Called after the confirmation delay when a match is confirmed to start the turn based loop.*/
	void StartConfirmedMatch();

	/* Called when the match ends before OnMatchEnd is called to finalise the state of turn based variables. 
	*
	* @param MatchResults Conclusion that the match ended with.
	*/
	void FinalizeEndMatch(const FTicTacToeMatchResults& MatchResults);

	/* Turn Based Match Tracking */

	int32 CurrentTurnCount;
	int32 EndingTurnCount;
	
	/* Results of the last finished game. Only valid if match has finished. */
	UPROPERTY(BlueprintReadOnly, Category = TurnBased)
	FTicTacToeMatchResults LastResults;

	/* Array of player states who will be participating in the match as players */
	TArray<class ATicTacToePlayerState*> ActivePlayers;

	/* Timer handle for starting a confirmed match after a delay. */
	FTimerHandle MatchStartTimer;

	/* Whether a match is currently in progress */
	uint32 bHasMatchStarted : 1;

	/* Whether a match has been started and finished and a new match hasn't started yet */
	uint32 bHasMatchFinished : 1;

public:
	/* Turn based delegates to bind events to. */

	/* Called when a new match is confirmed*/
	UPROPERTY(BlueprintAssignable, Category = TurnBased)
	FOnMatchConfirm OnMatchConfirm;

	/* Called when a confirmed match is cancelled before it starts */
	UPROPERTY(BlueprintAssignable, Category = TurnBased)
	FOnMatchCancel OnMatchCancel;

	/* Called when a confirmed match starts */
	UPROPERTY(BlueprintAssignable, Category = TurnBased)
	FOnMatchStart OnMatchStart;

	/* Called when a confirmed match ends */
	UPROPERTY(BlueprintAssignable, Category = TurnBased)
	FOnMatchEnd OnMatchEnd;

	/* Called when a round begins */
	UPROPERTY(BlueprintAssignable, Category = TurnBased)
	FOnRoundStart OnRoundStart;

	/* Called when a round ends */
	UPROPERTY(BlueprintAssignable, Category = TurnBased)
	FOnRoundEnd OnRoundEnd;

public:
	/* Returns LastResults */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category=TurnBased)
	FORCEINLINE FTicTacToeMatchResults GetLastResults() const { return LastResults; };

	/* Returns CurrentPlayerIndex */
	FORCEINLINE int32 GetCurrentPlayerIndex() const { return CurrentTurnCount % ActivePlayers.Num(); };
	/* Returns CurrentRoundIndex */
	FORCEINLINE int32 GetCurrentRoundIndex() const { return CurrentTurnCount / ActivePlayers.Num(); };
	/* Returns CurrentTurnCount */
	FORCEINLINE int32 GetCurrentTurnCount() const { return CurrentTurnCount; };
	/* Returns EndingPlayerIndex */
	FORCEINLINE int32 GetEndingPlayerIndex() const { return EndingTurnCount % ActivePlayers.Num(); };
	/* Returns EndingRoundIndex */
	FORCEINLINE int32 GetEndingRoundIndex() const { return EndingTurnCount / ActivePlayers.Num(); };
	/* Returns EndingTurnCount */
	FORCEINLINE int32 GetEndingTurnCount() const { return EndingTurnCount; };

	/* Returns player whose turn it is currently */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = TurnBased)
	FORCEINLINE ATicTacToePlayerState* GetCurrentPlayer() const { return ActivePlayers.IsValidIndex(GetCurrentPlayerIndex()) ? ActivePlayers[GetCurrentPlayerIndex()] : NULL; };

	/* Returns player whose turn the game was ended */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = TurnBased)
	FORCEINLINE ATicTacToePlayerState* GetEndingPlayer() const { return ActivePlayers.IsValidIndex(GetEndingPlayerIndex()) ? ActivePlayers[GetEndingPlayerIndex()] : NULL; };

	/* Returns turn index of the given player */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = TurnBased)
	FORCEINLINE int32 GetPlayerIndex(ATicTacToePlayerState* Player) { return ActivePlayers.Find(Player); }
};
