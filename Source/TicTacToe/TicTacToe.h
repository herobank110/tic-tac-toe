//     Tic Tac Toe    Copyright (C) 2019  David

#pragma once

#include "CoreMinimal.h"

/*********************************************
* Debugging output convenience macros 	     *
**********************************************/

#if WITH_EDITOR

#include "Logging/MessageLog.h"

/*Output an information message to the message log and output log for Play In Editor mode.*/
#define PIE_INFO(message) FMessageLog("PIE").Info(FText::FromString(message))
/*Output a formatted information message to the message log and output log for Play In Editor mode.*/
#define PIE_INFOF(message, ...) FMessageLog("PIE").Info(FText::FromString(FString::Printf(message, __VA_ARGS__)))

/*Output a warning message to the message log and output log for Play In Editor mode.*/
#define PIE_WARNING(message) FMessageLog("PIE").Warning(FText::FromString(message))
/*Output a formatted warning message to the message log and output log for Play In Editor mode.*/
#define PIE_WARNINGF(message, ...) FMessageLog("PIE").Warning(FText::FromString(FString::Printf(message, __VA_ARGS__)))

/*Output an error message to the message log and output log for Play In Editor mode.*/
#define PIE_ERROR(message) FMessageLog("PIE").Error(FText::FromString(message))
/*Output a formatted error message to the message log and output log for Play In Editor mode.*/
#define PIE_ERRORF(message, ...) FMessageLog("PIE").Error(FText::FromString(FString::Printf(message, __VA_ARGS__)))

#else

#define PIE_INFO
#define PIE_INFOF
#define PIE_WARNING
#define PIE_WARNINGF
#define PIE_ERROR
#define PIE_ERRORF

#endif // WITH_EDITOR
