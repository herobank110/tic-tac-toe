//     Tic Tac Toe    Copyright (C) 2019  David

#include "TicTacToeViewportClient.h"
#include "GameFramework/PlayerInput.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Engine.h"

UTicTacToeViewportClient::UTicTacToeViewportClient()
	: ActionToSendToAllLocals(TEXT("TriggerClick"))
	{}

bool UTicTacToeViewportClient::InputKey(const FInputKeyEventArgs& EventArgs)
{
	// Only send TriggerClick events to all players.
	// Send any other events by the normal means.

	// Check through all found mapping if the entered key is in it
	if (IsKeyInActionMapping(EventArgs.Key, ActionToSendToAllLocals))
	{
		UEngine* const Engine = GetOuterUEngine();
		int32 const NumPlayers = Engine ? Engine->GetNumGamePlayers(this) : 0;
		bool bRetVal = false;
		for (int32 i = 0; i < NumPlayers; i++)
		{
			const auto NewArgs = FInputKeyEventArgs(EventArgs.Viewport, i, EventArgs.Key, EventArgs.Event,
				EventArgs.AmountDepressed, EventArgs.bIsTouchEvent);			
			bRetVal = Super::InputKey(NewArgs) || bRetVal;
		}
		// Return true if any of the player controllers returned true.
		return bRetVal;
	}		

	// Otherwise use the default implementation to send this action to
	// only the player who caused it.
	return Super::InputKey(EventArgs);
}

bool UTicTacToeViewportClient::IsKeyInActionMapping(const FKey& Key, const FName& MappingName) const
{
	APlayerController* PC = GetWorld()->GetFirstPlayerController();

	// Get an array of input actions for the inputted MappingName
	TArray<FInputActionKeyMapping> Keys = PC->PlayerInput->GetKeysForAction(MappingName);
	// Check through all found mapping if the entered key is in it
	for (const FInputActionKeyMapping& InputMapping : Keys)
	{
		if (InputMapping.Key == Key)
		{
			return true; // Return true if found
		}
	}

	return false; //Otherwise return false, meaning the key wasn't found
}

