// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TicTacToeGameMode.h"
#include "TicTacToePlayerController.h"
#include "TicTacToePawn.h"
#include "TicTacToeGameState.h"
#include "TicTacToePlayerState.h"
#include "TicTacToeBlockGrid.h"
#include "TicTacToeBlock.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"
#include <Runtime\Engine\Public\EngineUtils.h>

#define LOCTEXT_NAMESPACE "PuzzleGameMode"

ATicTacToeGameMode::ATicTacToeGameMode()
{
	// no pawn by default
	DefaultPawnClass = ATicTacToePawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = ATicTacToePlayerController::StaticClass();
	// Use our own game state class
	GameStateClass = ATicTacToeGameState::StaticClass();
	// Use our own player state class
	PlayerStateClass = ATicTacToePlayerState::StaticClass();

	// Use 2 local players by default (set to 1 if using networked multiplayer)
	NumLocalPlayers = 2;
	NextPlayerStartIndex = 0;
	DefaultPlayerName = LOCTEXT("PlayerNameFmt", "Player {0}");

	// Turn based default configuration (override in blueprints!)
	ConfirmedMatchStartDelay = 3.f;
}

void ATicTacToeGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Create the necessary number of players for local play.
	for (int32 i = 1; i < NumLocalPlayers; ++i)
	{
		const auto NewPlayerController = UGameplayStatics::CreatePlayer(this);

	}

	//if (GEngine)
	//{
	//	const FString CanStart = CanStartMatch() ? "Can start" : "Cannot start";
	//	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Turquoise, CanStart);

	//	const FString CanEnd = CanEndMatch() ? "Can end" : "Cannot end";
	//	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Turquoise, CanEnd);
	//}
}

FString ATicTacToeGameMode::InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal)
{
	return Super::InitNewPlayer(NewPlayerController, UniqueId, Options,
		// Use player start with this index for spawn location, and increment the index.
		FString::FromInt(NextPlayerStartIndex++));
}

void ATicTacToeGameMode::GenericPlayerInitialization(AController* NewPlayer)
{
	// Set player name using default player name.
	// Since this using the next player start index, it will be
	// start from 1, so it is the player controller id + 1.
	const auto NewName = FText::Format(DefaultPlayerName, FText::AsNumber(NextPlayerStartIndex)).ToString();
	NewPlayer->PlayerState->SetPlayerName(NewName);
}

bool ATicTacToeGameMode::CanStartMatch_Implementation()
{
	// Require at least 2 players to start.
	return GetNumPlayers() >= 2;
}

bool ATicTacToeGameMode::CanEndMatch_Implementation()
{
	auto GameState = GetGameState<ATicTacToeGameState>();
	if (GameState)
	{
		// Get Block Grid actor.
		ATicTacToeBlockGrid* BlockGrid = NULL;
		for (TActorIterator<ATicTacToeBlockGrid> It(GetWorld(), ATicTacToeBlockGrid::StaticClass()); It; ++It)
		{
			BlockGrid = *It;
			break;
		}
		if (BlockGrid == NULL)
		{
			return false;
		}

		//TSet<ATicTacToePlayerState*> Owners;
		TMap<ATicTacToePlayerState*, int> Owners;
		ATicTacToeBlock* Block;
		int32 j = 0;

		// Check for any filled row (edge to edge).
		for (int32 i = 0; i < BlockGrid->Size; i++)
		{
			Owners.Reset();
			j = 0;
			for (; j < BlockGrid->Size; j++)
			{
				Block = BlockGrid->GetBlockAt(j, i);
				if (Block)
				{
					Owners.Add(Block->OwningPlayer, 1);
				}
			}
			if (Owners.Num() == 1 && !Owners.Contains(NULL))
			{
				// We found a fully occupied row with a single owner.
				LastMatchResults = FTicTacToeMatchResults(ETicTacToeWinMethod::Row, i);
				return true;
			}
		}

		// Check for any filled column (edge to edge).
		for (int32 i = 0; i < BlockGrid->Size; i++)
		{
			Owners.Reset();
			j = 0;
			for (; j < BlockGrid->Size; j++)
			{
				Block = BlockGrid->GetBlockAt(i, j);
				if (Block)
				{
					Owners.Add(Block->OwningPlayer, 1);
				}
			}
			if (Owners.Num() == 1 && !Owners.Contains(NULL))
			{
				// We found a fully occupied row with a single owner.
				LastMatchResults = FTicTacToeMatchResults(ETicTacToeWinMethod::Column, i);
				return true;
			}
		}

		// Check for a filled upward diagonal (corner to corner)
		Owners.Reset();
		for (int32 i = 0; i < BlockGrid->Size; i++)
		{
			Block = BlockGrid->GetBlockAt(i, i);
			if (Block)
			{
				Owners.Add(Block->OwningPlayer, 1);
			}
		}
		if (Owners.Num() == 1 && !Owners.Contains(NULL))
		{
			// We found a fully occupied diagonal with a single owner.
			// Index 0 for upward diagonal.
			LastMatchResults = FTicTacToeMatchResults(ETicTacToeWinMethod::Diagonal, 0);
			return true;
		}

		// Check for a filled downward diagonal (corner to corner)
		Owners.Reset();
		for (int32 i = 0; i < BlockGrid->Size; i++)
		{
			// Y-coordinate will go from 2 down to 0.
			j = (BlockGrid->Size - 1) - i;
			Block = BlockGrid->GetBlockAt(i, j);
			if (Block)
			{
				Owners.Add(Block->OwningPlayer, 1);
			}
		}
		if (Owners.Num() == 1 && !Owners.Contains(NULL))
		{
			// We found a fully occupied diagonal with a single owner.
			// Index 1 for downwarddiagonal. 
			LastMatchResults = FTicTacToeMatchResults(ETicTacToeWinMethod::Diagonal, 1);
			return true;
		}

		// Check if every tile on the board is filled (draw).
		if (GameState->GetCurrentTurnCount() >= BlockGrid->Size * BlockGrid->Size)
		{
			LastMatchResults = FTicTacToeMatchResults();
			return true;
		}
	}
	return false;
}

//FTicTacToeMatchResults GetMatchResults_Implementation()
//{
//	// Definitely override this!
//	return FTicTacToeMatchResults();
//}

#undef LOCTEXT_NAMESPACE
