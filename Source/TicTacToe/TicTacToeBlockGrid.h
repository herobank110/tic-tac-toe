// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TicTacToeBlockGrid.generated.h"

/** Class used to spawn blocks and manage score */
UCLASS(minimalapi)
class ATicTacToeBlockGrid : public AActor
{
	GENERATED_BODY()

	/** Dummy root component */
	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	/** Text component for the score */
	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTextRenderComponent* ScoreText;

	/** Map for tiles with their location as the key */
	TArray< TArray<class ATicTacToeBlock*> > BlocksMap;

public:
	ATicTacToeBlockGrid();

	/** How many blocks have been clicked */
	int32 Score;

	/** Number of blocks along each side of grid */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	int32 Size;

	/** Spacing of blocks */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	float BlockSpacing;

	/** Default class of blocks to create */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ATicTacToeBlock> BlockClass;

protected:
	// Begin AActor interface
	virtual void BeginPlay() override;
	// End AActor interface

public:

	/** Handle the block being clicked */
	void AddScore();

	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns ScoreText subobject **/
	FORCEINLINE class UTextRenderComponent* GetScoreText() const { return ScoreText; }
	/** Returns block in the grid at Position **/
	UFUNCTION(Category=Grid, BlueprintCallable)
	FORCEINLINE class ATicTacToeBlock* GetBlockAt(const FIntPoint& Position) const { return BlocksMap[Position.X][Position.Y]; }
	FORCEINLINE class ATicTacToeBlock* GetBlockAt(const int32 X, const int32 Y) const { return BlocksMap[X][Y]; }
};
