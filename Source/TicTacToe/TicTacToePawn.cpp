// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TicTacToePawn.h"
#include "TicTacToeBlock.h"
#include "TicTacToePlayerState.h"
#include "TicTacToePlayerController.h"
#include "TicTacToe.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

ATicTacToePawn::ATicTacToePawn(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	// With multiple players we want each one to have their own
	// player controller.
	//AutoPossessPlayer = EAutoReceiveInput::Player0;
}

void ATicTacToePawn::BeginPlay()
{
	Super::BeginPlay();

	// Only tick for hovering over tiles if player controlled.
	PrimaryActorTick.bCanEverTick = IsPlayerControlled();
}

void ATicTacToePawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// The actual controller owning this pawn (different in local multiplayer)
	auto OwnerPC = Cast<ATicTacToePlayerController>(Controller);

#if WITH_EDITOR || UE_BUILD_DEBUG
	if (GEngine)
	{
		auto OwnerPS = OwnerPC ? Cast<ATicTacToePlayerState>(OwnerPC->PlayerState) : NULL;
		auto BlockFocus = CurrentBlockFocus ? FString::Printf(TEXT("%d %d"), CurrentBlockFocus->GridPosition[0], CurrentBlockFocus->GridPosition[1]) : TEXT("_ _");
		auto Msg = FString::Printf(TEXT("Tick for %s | Block Focus %s"), *OwnerPS->GetPlayerName(), *BlockFocus);
		GEngine->AddOnScreenDebugMessage(-1, DeltaSeconds, FColor::Magenta, Msg);
	}
#endif

	//if (!OwnerPS->GetIsMyTurn())
	if(!OwnerPC || !OwnerPC->GetCanAcceptTurnInput())
	{
		// Disable highlight if it was on previously.
		if (CurrentBlockFocus)
		{
			CurrentBlockFocus->Highlight(false);
			CurrentBlockFocus = NULL;
		}
		return;
	}

	// Always use the first player controller as this acts as
	// the controls input for all players.
	auto HoverPC = GetWorld()->GetFirstPlayerController();
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (UCameraComponent* OurCamera = HoverPC->GetViewTarget()->FindComponentByClass<UCameraComponent>())
		{
			FVector Start = OurCamera->GetComponentLocation();
			FVector End = Start + (OurCamera->GetComponentRotation().Vector() * 8000.0f);
			TraceForBlock(Start, End, true);
		}
	}
	else
	{
		FVector Start, Dir, End;
		HoverPC->DeprojectMousePositionToWorld(Start, Dir);
		End = Start + (Dir * 8000.0f);
		TraceForBlock(Start, End, false);
	}
}

void ATicTacToePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("OnResetVR", EInputEvent::IE_Pressed, this, &ATicTacToePawn::OnResetVR);
	PlayerInputComponent->BindAction("TriggerClick", EInputEvent::IE_Pressed, this, &ATicTacToePawn::TriggerClick);
}

void ATicTacToePawn::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	Super::CalcCamera(DeltaTime, OutResult);

	OutResult.Rotation = FRotator(-90.0f, -90.0f, 0.0f);
}

void ATicTacToePawn::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATicTacToePawn::TriggerClick()
{
	if (CurrentBlockFocus)
	{
		auto PC = Cast<ATicTacToePlayerController>(Controller);
		if (PC->GetCanAcceptTurnInput())
		{
			CurrentBlockFocus->HandleClicked();
		}


	/*	auto PS = PC ? Cast<ATicTacToePlayerState>(PC->PlayerState) : NULL;
		if (PS->GetIsMyTurn())
		{
			CurrentBlockFocus->HandleClicked();
			PS->EndMyTurn();
		}*/
	}
}

void ATicTacToePawn::TraceForBlock(const FVector& Start, const FVector& End, bool bDrawDebugHelpers)
{
	FHitResult HitResult;
	GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_Visibility);
	if (bDrawDebugHelpers)
	{
		DrawDebugLine(GetWorld(), Start, HitResult.Location, FColor::Red);
		DrawDebugSolidBox(GetWorld(), HitResult.Location, FVector(20.0f), FColor::Red);
	}
	if (HitResult.Actor.IsValid())
	{
		ATicTacToeBlock* HitBlock = Cast<ATicTacToeBlock>(HitResult.Actor.Get());
		if (CurrentBlockFocus != HitBlock)
		{
			if (CurrentBlockFocus)
			{
				CurrentBlockFocus->Highlight(false);
			}
			if (HitBlock)
			{
				HitBlock->Highlight(true);
			}
			CurrentBlockFocus = HitBlock;
		}
	}
	else if (CurrentBlockFocus)
	{
		CurrentBlockFocus->Highlight(false);
		CurrentBlockFocus = nullptr;
	}
}